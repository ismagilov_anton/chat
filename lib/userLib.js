module.exports = function (io, rediscl) {
    /* #region  Sockets */
    io.on('connection', function (socket) {
        /* #region 🟢 Добавление нового пользователя */
        socket.on('addUser', function (username) {

            socket.username = username;
            socket.room = 'Lobby';

            // FIXME Redis к переменным не очень, заменить
            var username_for_redis = 'user_' + username;

            rediscl.rpush('users', username_for_redis);

            socket.join('Lobby');
            socket.emit('updateChat', '[SERVER]', 'Вы были подключены к Lobby');


            rediscl.lrange('rooms', 0, -1, function (err, rooms) {
                socket.emit('updateRooms', rooms, 'Lobby');
            });

            rediscl.lrange('messages_Lobby', 0, -1, function (err, oldmessages) {
                socket.emit('switchChat', oldmessages);
            });

            socket.broadcast.to('Lobby').emit('updateChat', '[SERVER]', username + ' был подключен к этой комнате');

        });
        /* #endregion */

        /* #region 🟢 Отправить сообщение */
        socket.on('sendMessage', function (message) {

            // FIXME Проблема строк в Redis
            var room_for_redis = 'messages_' + socket.room;

            rediscl.rpush(room_for_redis, message);

            io.sockets["in"](socket.room).emit('updateChat', socket.username, message);
        });
        /* #endregion */

        /* #region 🟢 Отключить пользователя */
        socket.on('disconnect', function () {


            var private_rooms = 'private_rooms_' + socket.username;
            var username = 'user_' + socket.username;

            rediscl.lrem('users', 0, username.toString());
            //TODO Удаление списка приватных комнат пользователя

            socket.broadcast.emit('updateChat', '[SERVER]', socket.username + ' отключился');
            socket.leave(socket.room);
            rediscl.lrange('users', 0, -1, function (err, usernames) {
                io.sockets.emit('updateUsers', usernames);
            });

        });
        /* #endregion */

        /* #region 🟢 Создание комнаты  */
        socket.on('createRoom', function (room, type) {
            // TODO Нет проверки на уникальное название комнаты

            var privat_room_for_redis = 'private_rooms_' + socket.username;

            switch (type) {
                case 'private': {
                    rediscl.rpush(privat_room_for_redis.toString(), room);
                    rediscl.lrange(privat_room_for_redis.toString(), 0, -1, function (err, private_rooms) {
                        socket.emit('updatePrivateRooms', private_rooms, socket.room);
                    });
                    break;
                }
                default: {
                    rediscl.rpush('rooms', room);

                    rediscl.lrange('rooms', 0, -1, function (err, rooms) {
                        io.sockets.emit('updateRooms', rooms, socket.room);
                    });
                    break;
                }
            }
        });
        /* #endregion */

        /* #region 🟢 Переключить комнату  */
        socket.on('switchRoom', function (newroom) {

            var oldroom;
            oldroom = socket.room;

            socket.leave(socket.room);
            socket.join(newroom);


            // TODO: Сделать отправку списка сообщений из комнаты по сокету
            socket.emit('updateChat', '[SERVER]', 'you have connected to ' + newroom);

            socket.broadcast.to(oldroom).emit('updateChat', '[SERVER]', socket.username + ' вышел из этой комнаты');
            socket.room = newroom;
            socket.broadcast.to(newroom).emit('updateChat', '[SERVER]', socket.username + ' вошел в эту комнату');

            var messages_for_redis = 'messages_' + newroom;

            rediscl.lrange(messages_for_redis, 0, -1, function (err, oldmessages) {
                socket.emit('switchChat', oldmessages);
            });

            rediscl.lrange('rooms', 0, -1, function (err, rooms) {
                socket.emit('updateRooms', rooms, newroom);
            });
        });
        /* #endregion */
    });
    /* #endregion */
};