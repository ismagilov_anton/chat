/* #region  Socket.io namespaces */

module.exports = function (io, rediscl) {
    const adminNamespace = io.of('/admin');

    adminNamespace.use((socket, next) => {
        // ensure the user has sufficient rights
        //TODO Аутентификация администратора
        next();
    });

    adminNamespace.on('connection', socket => {
        socket.on('push', function (message) {
            console.log(message);
        });
    }); 
    /* #endregion */
}

