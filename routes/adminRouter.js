const express = require("express");
const adminController = require("../controllers/adminController.js");
const adminRouter = express.Router();

adminRouter.use("/", adminController.admin);

module.exports = adminRouter;