
var express = require("express");
var app = require('express')();
var http = require('http').createServer(app);

var io = require('socket.io')(http);

app.use(express.static(__dirname + '/public'));

/* #region  HBS */
var hbs = require("hbs");
app.set("view engine", "hbs");
hbs.registerPartials(__dirname + "/views/layout");
/* #endregion */

/* #region  Routes */
const userRouter = require("./routes/userRouter.js");
const adminRouter = require("./routes/adminRouter.js");
app.use("/admin", adminRouter);
app.use("/", userRouter);
/* #endregion */

/* #region  Redis  */
const redis = require("redis");
var rediscl = redis.createClient();

rediscl.on("connect", function () {
    console.log("🟢 [REDIS] Redis подключен")

    // TODO: Возможно не здесь

    rediscl.flushall();

    rediscl.rpush('rooms', 'Lobby');
});

rediscl.on('error', function (error) {
    console.log('🔴 [REDIS] Redis не может подключится');
})
/* #endregion */

/* #region  Socket.io */
require('./lib/userLib')(io, rediscl);
require('./lib/adminLib')(io, rediscl);
/* #endregion */

http.listen(3000, function () {
    console.log(`🟢 [SERVER] Сервер запущен http://127.0.0.1:3000`)
})