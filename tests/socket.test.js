var io = require('socket.io-client');

var assert = require('assert');

var expect = require('chai').expect



describe('Suite of unit tests', function () {

    describe('First (hopefully useful) test', function () {

        it('socket connect', function (done) {
            var socket = io.connect('http://localhost:3000/admin');
            socket.on('connect', function () {
                console.log('worked...');
                done();
            });
            socket.on('push', function (data) {
                console.log(data);
                done();
            });
        });

        it('Doing some things with indexOf()', function (done) {
            expect([1, 2, 3].indexOf(5)).to.be.equal(-1);
            expect([1, 2, 3].indexOf(0)).to.be.equal(-1);
            done();
        });

    });
})