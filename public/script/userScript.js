var socket = io.connect('http://localhost:3000/');

socket.on('connect', function () {
    socket.emit('addUser', prompt("Введите ваше имя: "));
});

/* #region  Chat */
socket.on('updateChat', function (username, data) {
    $('#conversation').append('<div class="message">' + username + ':</b> ' + data + '<br>');
});

socket.on('switchChat', function (oldmessages) {

    $('#conversation').empty();

    oldmessages.forEach(function (message, i, oldmessages) {
        $('#conversation').append('<div class="message">' + ':</b> ' + message + '<br>');
    });

    //$('#conversation').append('<div class="message">' + ':</b> ' + oldmessages + '<br>');
});
/* #endregion */

/* #region  Rooms */
// TODO: Не кнопкки исправить
socket.on('updateRooms', function (rooms, current_room) {

    $('#rooms').empty();

    // TODO: Исправить внешний вид комнат
    $.each(rooms, function (key, value) {
        if (value == current_room) {
            $('#rooms').append('<div class="room_current">' + value + '</div>');
        }
        else {
            // TODO: Передлть в кнопку
            $('#rooms').append('<div class="room"><a href="#" onclick="switchRoom(\'' + value + '\')">' + value + '</a></div>');
        }
    });
});

socket.on('updatePrivateRooms', function (private_rooms, current_room) {

    $('#private_rooms').empty();

    // TODO: Исправить внешний вид комнат
    $.each(private_rooms, function (key, value) {
        if (value == current_room) {
            $('#private_rooms').append('<div class="room_current">' + value + '</div>');
        }
        else {
            // TODO: Передлть в кнопку
            $('#private_rooms').append('<div class="room"><a href="#" onclick="switchRoom(\'' + value + '\')">' + value + '</a></div>');
        }
    });
});

function switchRoom(room) {
    socket.emit('switchRoom', room);

    document.getElementById('parent').innerHTML = '<h3>Сейчас вы находитесь в: ' + room + '</h3>';

}
/* #endregion */

$(function () {

    $('#switchPrivateRoom').click(function () {


        if (!$('#privateroomname').val()) {
            alert('Название приватной комнаты не может быть пустым');
        }
        else {
            var private_room = $('#privateroomname').val();
            $('#privateroomname').val('');
            switchRoom(private_room);
        }
    });

    $('#datasend').click(function () {

        if (!$('#data').val()) {
            alert('Сообщение не может быть пустым');
        }
        else {
            var message = $('#data').val();
            $('#data').val('');
            socket.emit('sendMessage', message);
        }
    });

    $('#roombutton').click(function () {

        var type = $("input[name='type_room']:checked").val();
        var name = $('#roomname').val();

        if (!$('#roomname').val()) {
            alert('Название комнаты не может быть пустым');
        }
        else {
            $('#roomname').val('');
            $('#room_type').val('');

            socket.emit('createRoom', name, type)
        }
    });

    $('#data').keypress(function (e) {
        if (e.which == 13) {
            $(this).blur();
            $('#datasend').focus().click();
        }
    });

});